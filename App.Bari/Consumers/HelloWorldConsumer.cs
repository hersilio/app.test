﻿using App.Bari.Events;
using App.Core.EDA;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Bari.Consumers
{
    public class HelloWorldConsumer : BaseMessageConsumer<HelloWorldEvent>
    {
        protected override bool Consume(HelloWorldEvent @event)
        {
            Console.WriteLine(string.Format("Consume message: {0} / Id: {1} / Date: {2} / Service: {3}", 
                @event.Mensagem, 
                @event.EventConfig.EventId, 
                @event.EventConfig.Date, 
                ""));

            return true;
        }
    }
}
