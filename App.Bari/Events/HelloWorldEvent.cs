﻿using App.Core.EDA;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Bari.Events
{
    public class HelloWorldEvent : BaseEvent
    {
        public string Mensagem { get; set; }
    }
}
