﻿using App.Bari.Core.Services.Interfaces;
using App.Bari.Events;
using App.Bari.Services.Interfaces;
using App.Core.EDA;
using System;
using System.Threading;

namespace App.Bari.Services
{
    public class HelloWorldBizService : IBariBizService, IHelloWorldBizService
    {
        private readonly IEventPublisher _eventPublisher;

        public HelloWorldBizService(IEventPublisher eventPublisher)
        {
            _eventPublisher = eventPublisher;
        }

        public void SendMessages()
        {

            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromSeconds(5);

            var timer = new Timer((e) =>
            {
                RegisterMessageOnQueue();
            }, null, startTimeSpan, periodTimeSpan);
        }

        private void RegisterMessageOnQueue()
        {
            _eventPublisher.Send(new HelloWorldEvent()
            {
                Mensagem = "Hello World!"
            });
        }
    }
}
