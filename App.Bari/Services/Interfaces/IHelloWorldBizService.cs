﻿using App.Bari.Core.Services.Interfaces;

namespace App.Bari.Services.Interfaces
{
    public interface IHelloWorldBizService : IBariBizService
    {
        void SendMessages();
    }
}
