﻿using App.Bari.Core.Services.Interfaces;
using Serilog;

namespace App.Bari.Services
{
    public abstract class BariBizService : IBariBizService
    {
        public ILogger Logger { get; set; }
    }
}
