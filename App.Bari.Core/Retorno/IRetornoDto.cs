﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Bari.Core.Retorno
{
    public interface IRetornoDto
    {
        ResultadoOperacao Status { get; set; }
    }
}
