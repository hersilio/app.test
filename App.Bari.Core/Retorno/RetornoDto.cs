﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Bari.Core.Retorno
{
    public class RetornoDto<T> : IRetornoDto
    {
        public RetornoDto()
        {

        }

        public RetornoDto(T elemento)
        {
            Objeto = elemento;
        }

        public ResultadoOperacao Status { get; set; }

        public T Objeto { get; set; }
    }
}
