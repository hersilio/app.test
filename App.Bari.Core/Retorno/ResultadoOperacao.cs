﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Bari.Core.Retorno
{
    public enum ResultadoOperacao
    {
        Indefinido = 0,

        Sucesso = 1,

        Falha = 2,

        Alerta = 3,

        Info = 4
    }
}
