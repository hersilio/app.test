﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.EDA
{
    internal class EventAssignedConfig
    {
        public Type EventType { get; }

        public Type ConsumerType { get; }

        public int MsDelay { get; }

        public EventAssignedConfig(Type eventType, Type consumerType, int msDelay)
        {
            EventType = eventType;
            ConsumerType = consumerType;
            MsDelay = msDelay;
        }
    }
}
