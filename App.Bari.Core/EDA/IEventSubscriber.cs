﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.EDA
{
    public interface IEventSubscriber : IDisposable
    {
        void StartListener();
    }
}
