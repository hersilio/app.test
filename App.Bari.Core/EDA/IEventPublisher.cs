﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.EDA
{
    public interface IEventPublisher : IDisposable
    {
        void Send<TEvent>(TEvent @event) where TEvent : BaseEvent;

        void Send<TEvent, TOldEvent>(TEvent @event, TOldEvent oldEvent) where TEvent : BaseEvent where TOldEvent : BaseEvent;

        void SendAsJson<TEvent>(TEvent @event) where TEvent : BaseEvent;

        void SendDelayed<TEvent>(TEvent @event, TimeSpan delay) where TEvent : BaseEvent;

        void SendDelayed<TEvent, TOldEvent>(TEvent @event, TimeSpan delay, TOldEvent oldEvent) where TEvent : BaseEvent where TOldEvent : BaseEvent;
    }
}
