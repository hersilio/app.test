﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.EDA
{
    public abstract class BaseMessageConsumer<T> : IMessageConsumer where T : BaseEvent
    {
        protected abstract bool Consume(T @event);

        bool IMessageConsumer.Consume<T1>(T1 @event)
        {
            if (!(@event is T))
            {
                throw new ArgumentException("Tipo do evento não identificado", nameof(@event));
            }

            return Consume(@event as T);
        }
    }
}
