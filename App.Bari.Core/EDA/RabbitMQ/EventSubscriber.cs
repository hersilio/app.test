﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.EDA.RabbitMQ
{
    public class EventSubscriber : IEventSubscriber
    {
        internal IServiceProvider ServiceProvider { get; set; }

        internal IModel Channel { get; set; }

        public EventSubscriber(IConnection connection, IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
            Channel = connection.CreateModel();
        }

        public void StartListener()
        {
            var queue = Channel.ConfigureListenerQueue();

            var consumer = new EventingBasicConsumer(Channel);
            consumer.Received += (sender, args) =>
            {
                var ret = ExtensionsRabbitMQ.OnConsumerOnReceived(sender, args, ServiceProvider, Channel);

                if (ret)
                {
                    Channel.BasicAck(args.DeliveryTag, false);
                }
                else
                {
                    Channel.BasicNack(args.DeliveryTag, false, true);
                }
            };

            Channel.BasicConsume(queue: queue, consumer: consumer);
        }

        void IDisposable.Dispose()
        {
            if (Channel.IsOpen)
            {
                Channel.Close();
            }
        }
    }
}
