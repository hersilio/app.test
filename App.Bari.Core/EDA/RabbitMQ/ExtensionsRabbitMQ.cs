﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using App.Bari.Core.EDA;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace App.Core.EDA.RabbitMQ
{
    public static class ExtensionsRabbitMQ
    {
        private static bool _registeredFactory;

        internal static string ConfigureEventExchange<TEventType>(this IModel channel, bool delayed = false)
        {
            Type type = typeof(TEventType);
            return ConfigureEventExchange(channel, type, delayed);
        }
        public static void UseRabbitMQPublisher(this IServiceCollection serviceColletion, IConfiguration configuration)
        {
            serviceColletion.UseDefaultConnFactory(configuration);
            serviceColletion.AddScoped<IEventPublisher, EventPublisher>();
        }

        public static void UseRabbitMQSubscriber(this IServiceCollection serviceColletion, IConfiguration configuration)
        {
            serviceColletion.UseDefaultConnFactory(configuration);
            serviceColletion.AddSingleton<IEventSubscriber, EventSubscriber>();
            serviceColletion.AddSingleton<Microsoft.Extensions.Hosting.IHostedService, EventListenerService>();
        }

        private static void UseDefaultConnFactory(this IServiceCollection serviceColletion, IConfiguration configuration)
        {
            if (_registeredFactory)
            {
                return;
            }

            var sectionRabbitMQ = configuration.GetSection("RabbitMQConfig");
            ConnectionFactory connFactory = new ConnectionFactory();

            connFactory.UserName = sectionRabbitMQ.GetSection("UserName").Value;
            connFactory.Password = sectionRabbitMQ.GetSection("PassWord").Value;
            connFactory.VirtualHost = sectionRabbitMQ.GetSection("VirtualHost").Value;
            connFactory.HostName = sectionRabbitMQ.GetSection("HostName").Value;

            if (!string.IsNullOrWhiteSpace(sectionRabbitMQ.GetSection("Port").Value))
            {
                connFactory.Port = int.Parse(sectionRabbitMQ.GetSection("Port").Value);
            }


            var sd = new ServiceDescriptor(typeof(IConnection), connFactory.CreateConnection(sectionRabbitMQ.GetSection("ClientName").Value));
            serviceColletion.Add(sd);
            _registeredFactory = true;
        }

        internal static string ConfigureListenerQueue(this IModel channel)
        {
            var queues = new List<string>();
            var consumerQueueName = GetQueueName(Assembly.GetEntryAssembly());
            queues.Add(consumerQueueName);

            var logStashQueueName = GetLogStashQueueName();
            queues.Add(logStashQueueName);

            foreach (var queue in queues)
            {
                channel.QueueDeclare(queue, true, false, false);

                foreach (var assignedEvent in Extensions.AssignedEvents)
                {
                    var exchange = channel.ConfigureEventExchange(assignedEvent.Value.EventType);
                    channel.QueueBind(queue, exchange, assignedEvent.Key);
                }
            }

            return consumerQueueName;
        }

        public static bool OnConsumerOnReceived(object model, BasicDeliverEventArgs ea, IServiceProvider serviceProvider, IModel channel)
        {
            try
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);

                var type = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(a => a.GetTypes().Where(b => b.FullName == ea.RoutingKey))
                    .FirstOrDefault();

                if (type != null)
                {
                    using (var scope = serviceProvider.CreateScope())
                    {
                        var service = scope.ServiceProvider.GetConsumerService(type);
                        var methodInfo = typeof(IMessageConsumer).GetMethod("Consume", BindingFlags.Public | BindingFlags.Instance);
                        var makeGenericMethod = methodInfo.MakeGenericMethod(type);
                        var deserializeObject = JsonConvert.DeserializeObject(message, type);
                        return (bool)makeGenericMethod.Invoke(service, new[] { deserializeObject });
                    }
                }
            }
            catch (Exception e)
            {
                ea.BasicProperties.Headers.Add("ExceptionType", e.GetType().FullName);
                channel.BasicPublish(exchange: "App.Errors", routingKey: "App.Errors", basicProperties: ea.BasicProperties, body: ea.Body);
            }

            return true;
        }

        private static string ConfigureEventExchange(this IModel channel, Type type, bool delayed = false)
        {
            var configureExchange = GetExchangeName(type);

            if (delayed)
            {
                var dictionary = new Dictionary<string, Object> { { "x-delayed-type", ExchangeType.Direct } };
                var queue = GetQueueName(Assembly.GetEntryAssembly());
                var logStashQueueName = GetLogStashQueueName();

                var exchange = queue + ".Delayed";
                channel.ExchangeDeclare(exchange, "x-delayed-message", true, true, dictionary);
                channel.QueueDeclare(queue, true, false, false);
                channel.QueueBind(queue, exchange, type.FullName);
                channel.QueueBind(logStashQueueName, exchange, type.FullName);
                return exchange;
            }

            try
            {
                channel.ExchangeDeclare(configureExchange, ExchangeType.Topic, true);
            }
            catch(Exception ex)
            {

            }

            return configureExchange;
        }

        private static string GetExchangeName(Type type)
        {
            return type.Assembly.GetName().Name + ".Exchange";
        }

        private static string GetQueueName(Assembly assembly)
        {
            return assembly.GetName().Name + ".Queue";
        }

        private static string GetLogStashQueueName()
        {
            return "App.Logstash.Events.Queue";
        }
    }
}
