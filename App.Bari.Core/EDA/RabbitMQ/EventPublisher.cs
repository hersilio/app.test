﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Framing;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace App.Core.EDA.RabbitMQ
{
    public class EventPublisher : IEventPublisher
    {
        internal IModel Channel { get; set; }

        public EventPublisher(IConnection connection)
        {
            Channel = connection.CreateModel();
        }

        public void Send<TEvent>(TEvent @event) where TEvent : BaseEvent
        {
            var type = @event.GetType();
            var exchangeName = Channel.ConfigureEventExchange<TEvent>();
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(@event));

            
            IBasicProperties basic = new BasicProperties();
            basic.Headers = new Dictionary<string, object>();
            basic.Headers.Add("dataEnviada", DateTime.Now.ToString("o"));
            basic.Headers.Add("token", @event.EventConfig.Token);

            if (@event is IParameterizedEvent parameterizedEvent)
            {
                foreach (var parameter in parameterizedEvent.GetParameters())
                {
                    basic.Headers.Add(parameter.Key, parameter.Value);
                }
            }

            Channel.BasicPublish(exchange: exchangeName, routingKey: type.FullName, basicProperties: basic, body: body);
        }

        public void Send<TEvent, TOldEvent>(TEvent @event, TOldEvent oldEvent) where TEvent : BaseEvent where TOldEvent : BaseEvent
        {
            ConfigureEventsHistory(@event, oldEvent);

            Send(@event);
        }

        public void SendAsJson<TEvent>(TEvent @event) where TEvent : BaseEvent
        {
            var type = @event.GetType();
            var exchangeName = Channel.ConfigureEventExchange<TEvent>();
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(@event));

            IBasicProperties basic = new BasicProperties();
            basic.Headers = new Dictionary<string, object>();
            basic.Headers.Add("dataEnviada", DateTime.Now.ToString("o"));
            basic.Headers.Add("token", @event.EventConfig.Token);
            basic.ContentType = "application/json";

            if (@event is IParameterizedEvent parameterizedEvent)
            {
                foreach (var parameter in parameterizedEvent.GetParameters())
                {
                    basic.Headers.Add(parameter.Key, parameter.Value);
                }
            }

            Channel.BasicPublish(exchange: exchangeName, routingKey: type.FullName, basicProperties: basic, body: body);
        }

        public void SendDelayed<TEvent>(TEvent @event, TimeSpan delay) where TEvent : BaseEvent
        {
            var type = @event.GetType();

            var delayString = delay.TotalMilliseconds.ToString(CultureInfo.InvariantCulture);
            if (@event.EventConfig != null)
            {
                @event.EventConfig.Delayed = delayString;
            }

            var exchangeName = Channel.ConfigureEventExchange<TEvent>(delayed: true);
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(@event));

            IBasicProperties basic = new BasicProperties();
            basic.Headers = new Dictionary<string, object>();
            basic.Headers.Add("dataEnviada", DateTime.Now.ToString("o"));
            basic.Headers.Add("token", @event.EventConfig.Token);
            basic.Headers.Add("x-delay", delayString);
            Channel.BasicPublish(exchange: exchangeName, routingKey: type.FullName, basicProperties: basic, body: body);
        }

        public void SendDelayed<TEvent, TOldEvent>(TEvent @event, TimeSpan delay, TOldEvent oldEvent) where TEvent : BaseEvent where TOldEvent : BaseEvent
        {
            ConfigureEventsHistory(@event, oldEvent);

            SendDelayed(@event, delay);
        }

        void IDisposable.Dispose()
        {
            if (Channel.IsOpen)
            {
                Channel.Close();
            }
        }

        private void ConfigureEventsHistory<TEvent, TOldEvent>(TEvent @event, TOldEvent oldEvent) where TEvent : BaseEvent where TOldEvent : BaseEvent
        {
            if (@event.EventConfig == null || oldEvent.EventConfig == null)
            {
                return;
            }

            var historicoIds = string.IsNullOrEmpty(oldEvent.EventConfig.HistoryIds) ?
                oldEvent.EventConfig.EventId :
                $"{oldEvent.EventConfig.HistoryIds} -> {oldEvent.EventConfig.EventId}";

            if (string.IsNullOrEmpty(@event.EventConfig.HistoryIds))
            {
                @event.EventConfig.HistoryIds = historicoIds;
            }
            else
            {
                @event.EventConfig.HistoryIds += $" -> {historicoIds}";
            }

            var historicoTypes = string.IsNullOrEmpty(oldEvent.EventConfig.HistoryTypes) ?
                oldEvent.EventConfig.EventType :
                $"{oldEvent.EventConfig.HistoryTypes} -> {oldEvent.EventConfig.EventType}";

            if (string.IsNullOrEmpty(@event.EventConfig.HistoryTypes))
            {
                @event.EventConfig.HistoryTypes = historicoTypes;
            }
            else
            {
                @event.EventConfig.HistoryTypes += $" -> {historicoTypes}";
            }
        }
    }
}
