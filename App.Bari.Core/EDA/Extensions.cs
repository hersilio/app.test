﻿
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Core.EDA
{
    public static class Extensions
    {
        internal static readonly Dictionary<string, EventAssignedConfig> AssignedEvents;
        static Extensions()
        {
            AssignedEvents = new Dictionary<string, EventAssignedConfig>();
        }

        public static void SubscribeEvent<TEvent, TConsumer>(this IServiceCollection serviceCollection, int msDelay = 0)
            where TEvent : BaseEvent
            where TConsumer : class, IMessageConsumer
        {
            serviceCollection.AddScoped<IMessageConsumer, TConsumer>();
            AssignedEvents.Add(typeof(TEvent).FullName, new EventAssignedConfig(typeof(TEvent), typeof(TConsumer), msDelay));
        }

        public static void SubscribeTypedEvent<TEvent, TConsumer>(this IServiceCollection serviceCollection, int msDelay = 0)
            where TEvent : BaseEvent
            where TConsumer : BaseMessageConsumer<TEvent>
        {
            serviceCollection.AddScoped<IMessageConsumer, TConsumer>();
            AssignedEvents.Add(typeof(TEvent).FullName, new EventAssignedConfig(typeof(TEvent), typeof(TConsumer), msDelay));
        }

        internal static IMessageConsumer GetConsumerService(this IServiceProvider serviceProvider, Type eventType)
        {
            var service = serviceProvider.GetServices<IMessageConsumer>()
                .FirstOrDefault(a => a.GetType() == AssignedEvents[eventType.FullName].ConsumerType);

            return service;
        }

        // TODO Log Serilog
    }
}
