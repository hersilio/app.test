﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.EDA
{
    public abstract class BaseEvent
    {
        protected BaseEvent()
        {
            EventConfig = new EventConfig()
            {
                Date = DateTime.Now,
                Token = Guid.NewGuid().ToString(),
                EventId = Guid.NewGuid().ToString(),
                EventType = GetType().FullName
            };
        }

        public EventConfig EventConfig { get; set; }
    }
}
