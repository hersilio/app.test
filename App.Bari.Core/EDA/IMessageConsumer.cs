﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.EDA
{
    public interface IMessageConsumer
    {
        bool Consume<T>(T @event) where T : BaseEvent;
    }
}
