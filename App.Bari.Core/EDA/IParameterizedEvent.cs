﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Core.EDA
{
    public interface IParameterizedEvent
    {
        Dictionary<string, string> GetParameters();
    }
}
