﻿using App.Bari.Consumers;
using App.Bari.Events;
using App.Core.EDA;
using Microsoft.Extensions.DependencyInjection;

namespace App.Bari.Event
{
    public static class EventBuilder
    {
        public static void RegisterEvents(IServiceCollection services)
        {
            services.SubscribeTypedEvent<HelloWorldEvent, HelloWorldConsumer>();
        }
    }
}
