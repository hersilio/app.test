﻿using System;
using App.Bari.Core.Retorno;
using App.Bari.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace App.Bari.Event.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IHelloWorldBizService _helloWorldBizService;

        public HomeController(IHelloWorldBizService helloWorldBizService)
        {
            _helloWorldBizService = helloWorldBizService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            _helloWorldBizService.SendMessages();

            var resultado = new RetornoDto<bool>
            {
                Objeto = true,
                Status = ResultadoOperacao.Sucesso
            };

            return Ok(resultado);
        }
    }
}
